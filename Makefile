.DEFAULT_GOAL := help

include .env
HOST ?= localhost

help:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
.PHONY: help

##
## Setup
## -----
##

start: .env.local up info ## Start development environment

install: .env.local up
	docker-compose run composer install --ignore-platform-reqs --no-scripts
# 	docker-compose exec web bash -c "bin/console doctrine:schema:update --force"
	docker-compose exec web bash -c "bin/console doctrine:migrations:migrate --no-interaction"

.env.local: .env
	@if [ -f .env.local ]; then \
		echo '\033[1;41mYour .env.local file may be outdated because .env has changed.\033[0m';\
		echo '\033[1;41mCheck your .env.local file, or run this command again to ignore.\033[0m';\
		touch .env.local;\
		exit 1;\
	else\
		echo cp .env .env.local;\
		cp .env .env.local;\
	fi

up: ## Run containers
	docker-compose up -d

down: ## Stop and remove containers
	docker-compose down
