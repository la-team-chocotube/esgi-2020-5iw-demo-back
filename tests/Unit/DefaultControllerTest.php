<?php

declare(strict_types=1);

namespace App\Tests\Integration;

use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpClient\HttpClient;

final class DefaultControllerTest extends TestCase
{
    public function testListAllRoom()
    {
        $response = HttpClient::create()->request('GET', $_ENV['BASE_URL'].'/');
        $statusCode = $response->getStatusCode();
        $this->assertEquals(200, $statusCode);
        $this->assertEquals('application/json', $response->getInfo()['content_type'] ?? null);
    }
}
