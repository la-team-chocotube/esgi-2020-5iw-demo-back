<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Todo;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="showtodos", methods={"GET"})
     */
    public function showTodos(): Response
    {
        $repository = $this->getDoctrine()->getRepository(Todo::class);

        $todos = $repository->findAll();

        $data = $this->get('serializer')->serialize($todos, 'json');

        $response = new Response($data);

        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @Route("/", name="addtodo", methods={"POST"})
     *
     * @return Response
     */
    public function addTodo(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $data = [];
        if ($content = $request->getContent()) {
            $data = json_decode($content, true);
        }

        $todo = new Todo();

        $todo->setTitle($data['Title']);
        $todo->setCompleted($data['Completed']);

        $entityManager->persist($todo);
        $entityManager->flush();

        return new Response('Saved new todo with id '.$todo->getId());
    }
}
